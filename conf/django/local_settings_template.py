# -*- coding: utf-8 -*-
from local_settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

MEDIA_URL = '/media/'
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'

INSTALLED_APPS += (

)
