django-tunes
============

Mac OS X has a Scripting Bridge framework that allows you to talk to native
apps using native APIs. I did a working proof-of-concept iTunes Web remote
back in 2008 ( http://www.flickr.com/photos/uninen/2448444839/ ), lost the
code, and now I'm writing it again.

The interesting code lives in `pylib/tunes/lib.py`

NB! This works only on Mac OS X 10.5+.
